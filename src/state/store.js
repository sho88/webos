import createStore from "redux-zero";

const initialState = {
  date: null,
  installedApps: [ // should come via "/api"
    { _id: '_s0d98fuisdf0d', title: 'Control Panel', icon: 'fab fa-cpanel' },
    { _id: '_dfpogdfg9sfdo', title: 'Blog', icon: 'fas fa-blog' },
    { _id: '_vdfjg089gfd0g', title: 'Note Taker', icon: 'fas fa-sticky-note' },
    { _id: '_ldfogidfg0988', title: 'eLearning', icon: 'fas fa-graduation-cap' },
    { _id: '_kjdfdfsofidsf', title: 'Business Resources Consortium', icon: 'fas fa-business-time' },
    { _id: '_kjdfdfsofidsw', title: 'Music', icon: 'fas fa-music' },
    { _id: '_kjdfdfsofidsq', title: 'Youtube', icon: 'fab fa-youtube' },
  ],
  numberOfClicks: 0,
  openedApps: [],
  selectedApp: null,
  showStartMenu: false,
  time: null,
};
const store = createStore(initialState);

export default store;
