const actions = store => ({
  setDate: (state, time) => ({ time }),
  setNumberOfClicks: (state, numberOfClicks) => ({ numberOfClicks }),
  setSelectedApplication: (state, selectedApp) => {
    const openedApps = state.openedApps.filter(app => app._id === selectedApp._id).length < 1
      ? [...state.openedApps, selectedApp]
      : state.openedApps;

    return {
      openedApps,
      selectedApp,
      showStartMenu: false
    };
  },
  setTime: (state, date) => ({ date }),
  showDesktop: state => ({ selectedApp: null }),
  toggleStartMenu: (state, showStartMenu) => ({ showStartMenu: !state.showStartMenu }),
})

export default actions;
