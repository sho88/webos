import React, {Component} from 'react';
import {connect} from 'redux-zero/react';
import actions from '../../state/actions';
import './Program.css';
import Blog from "../Apps/Blog/Blog";
import ControlPanel from "../Apps/ControlPanel/ControlPanel";
import ELearning from "../Apps/ELearning/ELearning";
import NoteTaker from "../Apps/NoteTaker/NoteTaker";
import Youtube from "../Apps/Youtube/Youtube";

class Program extends Component {
    onAction = type => type === 'close'
      ? this.props.showDesktop()
      : console.log('Minimize...');

    render() {
        let component = null;
        switch (this.props.selectedApp.title) {
            case 'Blog':
              component = <Blog/>;
              break;
            case 'Control Panel':
              component = <ControlPanel/>;
              break;
            case 'eLearning':
              component = <ELearning/>;
              break;
            case 'Note Taker':
              component = <NoteTaker/>;
              break;
            case 'Youtube':
              component = <Youtube/>;
              break;
            default:
              component = null;
        }

        return (
          <div className="program">
            <div className="program__bar">
              <div className="name">{this.props.selectedApp.title}</div>
              <div className="action" onClick={() => this.onAction('close')}><i className="fas fa-times"></i>
              </div>
              <div className="action" onClick={() => this.onAction('minimize')}>
                <i className="fas fa-angle-double-down"></i>
              </div>
            </div>
            {component}
          </div>
        );
    }

}

const mapToProps = ({selectedApp, showDesktop}) => ({selectedApp, showDesktop});
export default connect(mapToProps, actions)(Program);
