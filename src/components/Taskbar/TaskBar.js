import React from 'react';
import { connect } from 'redux-zero/react';
import './TaskBar.css';
import startLogo from './../../2019-logo.png'
import actions from './../../state/actions';

const mapToProps = ({ date, openedApps, time, toggleStartMenu }) => ({
  date, openedApps, time, toggleStartMenu
});

export default connect(mapToProps, actions)(({
  date,
  openedApps,
  setSelectedApplication,
  time,
  toggleStartMenu
}) => (
  <div className="taskbar">
    <div className="taskbar__start">
      <div onClick={ () => toggleStartMenu() }>
        <img alt="Start" src={startLogo} />
      </div>
    </div>

    <div className="taskbar__mid">
      <div>
        <ul>
          {
            openedApps.map(app => (
              <li key={app._id} onClick={() => setSelectedApplication(app)}>
                <em className={app.icon}></em>
              </li>
            ))
          }
        </ul>
      </div>
    </div>

    <div className="taskbar__date">
      <div>
        <p>{time}<br />{date}</p>
      </div>
    </div>
  </div>
));
