import React, {useEffect, useState} from 'react';
import './../apps.css';

function NoteTaker () {
  const [notes, setNotes] = useState([]);
  const [formData, setFormData] = useState({ content: '', title: '' });

  const onChangeValue = e => {
    formData = {
      ...formData,
      [e.target.getAttribute('name')]: e.target.value.trim()
    }
    console.log(formData)
  }

  const onSubmission = e => {
    const data = new FormData(e.target)
    console.log(data)
    e.preventDefault()
  }

  useEffect(() => console.log('NoteTakerComponent.'), [])

  return (
    <div className="Apps-container">
      <div className="">
        <form onSubmit={e => onSubmission(e)}>
          <label htmlFor="title">Title</label>
          <input name="title" onChange={e => onChangeValue(e)} />
          <hr />
          <label htmlFor="content">Content</label>
          <textarea name="content" onChange={e => onChangeValue(e)}></textarea>
          <hr />
          <button>Add</button>
        </form>
      </div>

      {
        notes.length > 0
        ? notes.map(item => <div className="">{item}</div>)
        : null
      }
    </div>
  );
}

export default NoteTaker;
