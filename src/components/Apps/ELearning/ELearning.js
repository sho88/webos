import React, {useState} from 'react';

export default () => {
  const [name, setName] = useState('');

  return (
    <div>
      <p onClick={() => setName('Sho-Silva Carter-Daniel')}>Name: {name}</p>
    </div>
  )
}
