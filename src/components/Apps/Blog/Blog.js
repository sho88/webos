import React, {useEffect, useState} from 'react';
import axios from 'axios';
import {connect} from 'redux-zero/react';
import actions from '../../../state/actions';
import User from '../../User/User';

function Blog ({ setNumberOfClicks, numberOfClicks }) {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    axios.get('https://jsonplaceholder.typicode.com/users')
      .then(({ data: users }) => setUsers(users))
      .catch(err => console.log('Err', err));
  }, []);

  return (
    <div>
      <p onClick={() => setNumberOfClicks(numberOfClicks+1)}>This is the BlogComponent... {numberOfClicks}</p>
      {users ? users.map(user => <User key={user.id} data={user} />) : null}
    </div>
  );
}

const mapToProps = ({ setNumberOfClicks, numberOfClicks }) => ({ setNumberOfClicks, numberOfClicks });
export default connect(mapToProps, actions)(Blog);
