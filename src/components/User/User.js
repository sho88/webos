import React from 'react';

function User ({ data }) {
  const { id, name, email, phone, username, website } = data;

  return (
    <div id={`user-${id}`} style={{ background: '#f1f1f1', marginBottom: '10px', padding: '10px' }}>
      <h4>{name} <span style={{ color: 'gray', fontSize: '12px', fontWeight: 'normal' }}>(@{username})</span></h4>
      <p>Phone: {phone}</p>
      <p>Email: {email}</p>
      <p>Website: {website}</p>
    </div>
  );
}

export default User;
