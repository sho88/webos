import React from 'react';
import {connect} from 'redux-zero/react';
import actions from '../../state/actions';
import './Tile.css';

const mapToProps = ({setSelectedApplication}) => ({setSelectedApplication});

export default connect(mapToProps, actions)(({tile, setSelectedApplication}) => {
    const openApplication = tile => setSelectedApplication(tile);

    return (
        <div className="app" onClick={() => openApplication(tile)}>
            <div className="app__content">
                <div>
                    <h4>
                      <em className={tile.icon}></em>
                      {tile.title}
                    </h4>
                    <p>Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries
                        for previewing layouts and visual mockups.</p>
                </div>
            </div>
        </div>
    );
});
