import React, {Component} from 'react';
import { connect } from 'redux-zero/react';
import actions from './../../state/actions';
import './StartMenu.css';
import Tile from './../Tile/Tile';

const mapToProps = ({ installedApps }) => ({ installedApps });

class StartMenu extends Component {
  render() {
    const apps = this.props.installedApps.map((app, i) => <Tile key={i} tile={app} />);

    return (
      <div className="start-menu">
        <div className="start-menu__app">{apps}</div>
      </div>
    )
  }
}

export default connect(mapToProps, actions)(StartMenu);
