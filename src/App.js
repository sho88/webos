import React, {useEffect} from 'react';
import {connect} from "redux-zero/react";
import actions from './state/actions';
import './App.css';
import moment from 'moment';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import StartMenu from './components/StartMenu/StartMenu';
import TaskBar from './components/Taskbar/TaskBar';
import Program from './components/Program/Program';

function App ({ date, time, setDate, selectedApp, setTime, showStartMenu }) {
  useEffect(() => {
    function onTimeUpdate () {
      const m = moment();
      setDate(m.format('DD/MM/YYYY'));
      setTime(m.format('H:mm:ss'));
    }

    setInterval(() => onTimeUpdate(), 1000);
  }, [date, time])

  const startMenu = showStartMenu ? <StartMenu /> : null;

  return (
    <div className="application">
      <div className="pane">
        <ReactCSSTransitionGroup
          transitionName="example"
          transitionEnterTimeout={400}
          transitionLeaveTimeout={400}>
        {startMenu}
        </ReactCSSTransitionGroup>
        {selectedApp ? <Program /> : null}
      </div>
      <TaskBar />
    </div>
  );
}

const mapToProps = ({ date, time, setDate, selectedApp, setTime, showStartMenu }) => ({ date, time, setDate, selectedApp, setTime, showStartMenu });
export default connect(mapToProps, actions)(App);
